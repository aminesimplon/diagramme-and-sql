-- Active: 1709545637393@@127.0.0.1@3306@exo_simplon

DROP TABLE IF EXISTS promo;
DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS centerV;
DROP TABLE IF EXISTS region;
DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS booking;
DROP TABLE IF EXISTS promo_student;

CREATE TABLE region(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL
);

CREATE TABLE centerV(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL,
    regionId INT,
    FOREIGN KEY (regionId) REFERENCES region(id)
);

CREATE TABLE student(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL
);

CREATE TABLE promo (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (64) NOT NULL,
    startDate DATE,
    endDate DATE,
    studentNumber INT,
    centerVId INT,
    FOREIGN KEY (centerVId) REFERENCES centerV(id)
);


CREATE TABLE room(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL,
    capacity INT NOT NULL,
    color VARCHAR(64) NOT NULL,
    centerVId INT,
    FOREIGN KEY (centerVId) REFERENCES centerV(id)
);

CREATE TABLE booking(
    id INT PRIMARY KEY AUTO_INCREMENT, 
    startDate DATE,
    endDate DATE,
    roomId INT,
    FOREIGN KEY (roomId) REFERENCES room(id),
    promoId INT,
    FOREIGN KEY (promoId) REFERENCES promo(id)
);

CREATE TABLE promo_student(
    studentId INT,
    promoId INT,
    PRIMARY KEY (promoId, studentId),
    FOREIGN KEY (promoId) REFERENCES promo(id),
    FOREIGN KEY (studentId) REFERENCES student(id)
);

INSERT INTO region (name) VALUES ('Rhone'), ('Ile de france'), ('Lorraine');
INSERT INTO student (name) VALUES ('Amine'), ('Jack'), ('Greg'), ('Matt');

INSERT INTO promo (name, startDate, endDate, studentNumber, centerVId) VALUES 
    ('Promo A', '2024-01-01', '2024-06-30', 30, 1), -- Promo A à Lyon
    ('Promo B', '2024-02-01', '2024-07-31', 25, 2), -- Promo B à Paris
    ('Promo C', '2024-03-01', '2024-08-31', 35, 3); -- Promo C à Nancy

-- 
INSERT INTO room (name, capacity, color, centerVId) VALUES 
    ('Room 101', 20, 'Blue', 1),    -- Salle à Lyon
    ('Room 201', 25, 'Red', 2),    -- Salle à Paris
    ('Room 301', 30, 'Green', 3);  -- Salle à Nancy

INSERT INTO promo_student (studentId, promoId) VALUES 
    (9, 1),
    (10, 2), 
    (11, 3), 
    (12, 1);

INSERT INTO centerV (name, regionId) VALUES 
    ('Centre de formation Lyon', 10), 
    ('École Parisienne', 12),         
    ('Institut Nancy', 11),
    ('Centre de formation Marseille', 11),  
    ('École Toulousaine', 11),              
    ('Institut Strasbourg', 12),            
    ('Centre de formation Bordeaux', 11),    
    ('École Lilloise', 12),                 
    ('Institut Montpellier', 12),           
    ('Centre de formation Rennes', 12),      
    ('École Nantaise', 10),             
    ('Institut Grenoble', 11),          
    ('Centre de formation Nice', 12);   
