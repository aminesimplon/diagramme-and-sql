-- Active: 1709545637393@@127.0.0.1@3306@exo_simplon

-- Gerer les salles > lister les salles d'un centre
SELECT room.* FROM room JOIN centerV ON room.centerVId = centerV.id WHERE centerV.id = 1;

-- Gerer les salles > assigner une capaciter a une salle
UPDATE room SET capacity = 30 WHERE id = 2;

-- Gerer les centre > Lister les centres d'une region
SELECT * FROM centerV WHERE regionId = 10;

-- Gerer les promos > lister les promos d'un centre
SELECT * FROM promo WHERE centerVId = 2;

-- Afficher le planning complet > par salle
SELECT * FROM booking WHERE roomId = 1;

-- Afficher le planning complet > par promo
SELECT * FROM booking WHERE promoId = 3;
